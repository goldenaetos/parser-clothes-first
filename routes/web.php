<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\ParserController;
use App\Http\Controllers\Admin\ParsingSetController;
use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/admin', function () {
    $pageTitle = 'Панель Адміністратора';
    $breadcrumbs = [
        ['title' => 'Панель Адміністратора', 'route' => route('admin')]
    ];
    return view('admin.dashboard', compact('pageTitle', 'breadcrumbs'));
})->name('admin');


Route::prefix('admin/categories')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])->name('categories.index');
    Route::get('/create', [CategoryController::class, 'create'])->name('categories.create');
    Route::post('/', [CategoryController::class, 'store'])->name('categories.store');
    Route::get('/{category}', [CategoryController::class, 'show'])->name('categories.show');
    Route::get('/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
    Route::put('/{category}', [CategoryController::class, 'update'])->name('categories.update');
    Route::delete('/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');
    Route::post('/set-warning-message', [CategoryController::class, 'setWarningMessage'])->name('categories.setWarningMessage');
});

Route::prefix('admin/sets')->group(function () {
    Route::get('/', [ParsingSetController::class, 'index'])->name('sets.index');
    Route::get('/create', [ParsingSetController::class, 'create'])->name('sets.create');
    Route::post('/', [ParsingSetController::class, 'store'])->name('sets.store');
    Route::get('/{parsingSet}', [ParsingSetController::class, 'show'])->name('sets.show');
    Route::get('/{parsingSet}/edit', [ParsingSetController::class, 'edit'])->name('sets.edit');
    Route::put('/{parsingSet}', [ParsingSetController::class, 'update'])->name('sets.update');
    Route::delete('/{parsingSet}', [ParsingSetController::class, 'destroy'])->name('sets.destroy');
});

Route::prefix('admin/pages')->group(function () {
    Route::get('/', [PageController::class, 'index'])->name('pages.index');
    Route::get('/create', [PageController::class, 'create'])->name('pages.create');
    Route::post('/', [PageController::class, 'store'])->name('pages.store');
    Route::get('/{page}', [PageController::class, 'show'])->name('pages.show');
    Route::get('/{page}/edit', [PageController::class, 'edit'])->name('pages.edit');
    Route::put('/{page}', [PageController::class, 'update'])->name('pages.update');
    Route::delete('/{page}', [PageController::class, 'destroy'])->name('pages.destroy');
});

Route::fallback(function () {
    if (request()->is('admin/*')) {
        return Redirect::route('admin')->with('error', 'Сторінку не знайдено.');
    } else {
        return Redirect::route('home')->with('error', 'Сторінку не знайдено.');
    }
});

Route::prefix('articles')->group(function () {
    Route::get('/', [ArticleController::class, 'index'])->name('articles.index');
    Route::get('/{article}', [ArticleController::class, 'show'])->name('articles.show');
    Route::get('/ru/{article}', [ArticleController::class, 'showRu'])->name('articles.show_ru');
    Route::get('/ua/{article}', [ArticleController::class, 'showUa'])->name('articles.show_ua');
    Route::delete('/{article}', [ArticleController::class, 'destroy'])->name('articles.destroy');
});

//Route::get('test', [ParserController::class, 'dispatchParseArticleJob']);


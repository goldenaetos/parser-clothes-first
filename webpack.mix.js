const mix = require('laravel-mix');
const glob = require('glob');
const path = require('path');

function getFiles(dir) {
    return glob.sync(`${dir}/**/*.js`).map(file => ({
        source: file,
        destination: file.replace('resources', 'public')
    }));
}

const adminFiles = getFiles('resources/js/admin');

adminFiles.forEach(file => {
    mix.js(file.source, path.dirname(file.destination));
});

mix.js('resources/js/app.js', 'public/js')
    .css('resources/css/app.css', 'public/css')
    .babelConfig({
        presets: ['@babel/preset-env'],
    })
    .sourceMaps();

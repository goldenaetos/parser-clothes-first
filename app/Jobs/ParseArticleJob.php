<?php

namespace App\Jobs;

use App\Services\Admin\ParserService;
use App\Services\ArticleService;
use App\Services\TranslateService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ParseArticleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $translateApiKey;

    public function __construct($translateApiKey)
    {
        $this->translateApiKey = $translateApiKey;
    }

    public function handle(ParserService $parserService, ArticleService $articleService)
    {
        DB::beginTransaction();

        try {
            $translateService = new TranslateService($this->translateApiKey);
            $result = $parserService->processRandomPage([]);

            if (!isset($result['randomUrl'])) {
                DB::commit();
                return;
            }

            $articleData = $parserService->fetchArticleContent($result['randomUrl'], $result['parsing_set_id'], $result['page']);

            if ($articleData['content'] === 'Content not found') {
                DB::commit();
                ParseArticleJob::dispatch($this->translateApiKey);
                return;
            }

            if ($articleData['title'] === 'Error fetching the content') {
                DB::commit();
                ParseArticleJob::dispatch($this->translateApiKey);
                return;
            }

            if ($parserService->shouldSkipArticle($articleData['content'], $result['parsing_set_id'], $result['randomUrl'])) {
                DB::commit();
                ParseArticleJob::dispatch($this->translateApiKey);
                return;
            }

            $articletWithTitleAndContent = $articleService->createArticle($articleData, $result['page'], $result['randomUrl']);
            $article = $articletWithTitleAndContent[0];
            $title = $articletWithTitleAndContent[1];
            $content = $articletWithTitleAndContent[2];

            $translatedArticleContent = $articleService->translateAndFormat($title, $content, $translateService);

            $articleService->saveTranslation($article, $translatedArticleContent);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to parse article at URL: ' . ($result['randomUrl'] ?? 'URL not set') . '. Error: ' . $e->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Constants\ParsingSetConstants;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ParsingSetRequest;
use App\Models\Admin\Category;
use App\Models\Admin\Page;
use App\Models\Admin\ParsingSet;
use App\Services\Admin\ParsingSetService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ParsingSetController extends Controller
{
    public function __construct(
        protected ParsingSetService $parsingSetService
    ) {}

    public function index()
    {
        $parsingSets = ParsingSet::all();
        $warnings = $this->parsingSetService->getDuplicateSlugsWarning();
        $pageTitle = 'Комплекти для парсингу';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.parsing_sets.index', compact('parsingSets', 'warnings', 'pageTitle', 'breadcrumbs'));
    }

    public function create()
    {
        $categories = Category::all();
        $elements = ParsingSetConstants::getElements();
        $stringElements = ParsingSetConstants::getStringElements();
        $separateElements = ParsingSetConstants::getSeparateElements();
        $pageTitle = 'Додати комплект для парсингу';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Комплекти для парсингу', 'route' => route('sets.index')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.parsing_sets.create', compact('categories', 'elements', 'stringElements', 'separateElements', 'pageTitle', 'breadcrumbs'));
    }

    public function store(ParsingSetRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $this->parsingSetService->prepareData($request->validated());
            $data['slug'] = Str::slug($data['set_name']);
            $parsingSet = ParsingSet::create($data);

            if ($request->has('pages')) {
                foreach ($data['pages'] as $page) {
                    Page::create([
                        'url' => $page['url'],
                        'category_id' => $page['category_id'],
                        'parsing_set_id' => $parsingSet->id,
                    ]);
                }
            }

            DB::commit();
            return redirect()->route('sets.index')->with('success', 'Parsing Set created successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to create parsing set ' . $e->getMessage());
            return redirect()->route('sets.index')->with('error', 'Failed to create parsing set.');
        }
    }

    public function show(ParsingSet $parsingSet)
    {
        $elements = ParsingSetConstants::getElements();
        $stringElements = ParsingSetConstants::getStringElements();
        $separateElements = ParsingSetConstants::getSeparateElements();
        $pageTitle = $parsingSet->set_name;

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Комплекти для парсингу', 'route' => route('sets.index')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.parsing_sets.show', compact('parsingSet', 'elements', 'stringElements', 'separateElements', 'pageTitle', 'breadcrumbs'));
    }

    public function edit(ParsingSet $parsingSet)
    {
        $categories = Category::all();
        $pages = $parsingSet->pages()->with('category')->get();
        $elements = ParsingSetConstants::getElements();
        $stringElements = ParsingSetConstants::getStringElements();
        $separateElements = ParsingSetConstants::getSeparateElements();
        $pageTitle = 'Редагувати комплект';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Комплекти для парсингу', 'route' => route('sets.index')],
            ['title' => $parsingSet->set_name, 'route' => route('sets.show', $parsingSet->slug)],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.parsing_sets.edit', compact('parsingSet', 'categories', 'pages', 'elements', 'stringElements', 'separateElements', 'pageTitle', 'breadcrumbs'));
    }

    public function update(ParsingSetRequest $request, ParsingSet $parsingSet)
    {
        DB::beginTransaction();
        try {
            $data = $this->parsingSetService->prepareData($request->validated());
            $data['slug'] = Str::slug($data['set_name']);
            $parsingSet->update($data);

            $parsingSet->pages()->delete();
            if ($request->has('pages')) {
                foreach ($request->pages as $pageData) {
                    if (isset($pageData['url']) && isset($pageData['category_id'])) {
                        $parsingSet->pages()->create([
                            'url' => $pageData['url'],
                            'category_id' => $pageData['category_id'],
                        ]);
                    }
                }
            }

            DB::commit();
            return redirect()->route('sets.index')->with('success', 'Parsing Set updated successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to update parsing set: ' . $e->getMessage());
            return redirect()->route('sets.index')->with('error', 'Failed to update parsing set.');
        }
    }

    public function destroy(ParsingSet $parsingSet)
    {
        DB::beginTransaction();
        try {
            $parsingSet->delete();
            DB::commit();
            return redirect()->route('sets.index')->with('success', 'Parsing Set deleted successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to delete parsing set ' . $e->getMessage());
            return redirect()->route('sets.index')->with('error', 'Failed to delete parsing set.');
        }
    }
}

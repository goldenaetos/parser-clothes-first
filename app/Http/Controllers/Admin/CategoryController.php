<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Category;
use App\Services\Admin\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    public function __construct(
        protected CategoryService $categoryService
    ) {}

    public function index()
    {
        $categories = Category::all();
        $warnings = $this->categoryService->getDuplicateSlugsWarning();
        $pageTitle = 'Категорії';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.categories.index', compact('categories', 'warnings', 'pageTitle', 'breadcrumbs'));
    }

    public function create()
    {
        $pageTitle = 'Додати категорію';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Категорії', 'route' => route('categories.index')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.categories.create', compact('pageTitle', 'breadcrumbs'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->validate([
                'name' => [
                    'required',
                    Rule::unique('categories', 'name')->whereNull('deleted_at')
                ]
            ]);
            $data['slug'] = Str::slug($data['name']);
            Category::create($data);
            DB::commit();
            return redirect()->route('categories.index')->with('success', 'Category created successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('categories.index')->with('error', 'Failed to create category.');
        }
    }

    public function show(Category $category)
    {
        $pageTitle = $category->name;

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Категорії', 'route' => route('categories.index')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.categories.show', compact('category', 'pageTitle', 'breadcrumbs'));
    }

    public function edit(Category $category)
    {
        $pageTitle = 'Редагувати категорію';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Категорії', 'route' => route('categories.index')],
            ['title' => $category->name, 'route' => route('categories.show', $category->slug)],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.categories.edit', compact('category', 'pageTitle', 'breadcrumbs'));
    }

    public function update(Request $request, Category $category)
    {
        DB::beginTransaction();
        try {
            $data = $request->validate([
                'name' => [
                    'required',
                    Rule::unique('categories', 'name')->ignore($category->id)->whereNull('deleted_at')
                ]
            ]);
            $data['slug'] = Str::slug($data['name']);
            $category->update($data);
            DB::commit();
            return redirect()->route('categories.index')->with('success', 'Category updated successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('categories.index')->with('error', 'Failed to update category.');
        }
    }

    public function destroy(Category $category)
    {
        DB::beginTransaction();
        try {
            $category->delete();
            DB::commit();
            return redirect()->route('categories.index')->with('success', 'Category deleted successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect('categories.index')->with('error', 'Failed to delete category.');
        }
    }

    public function setWarningMessage(Request $request)
    {
        session(['warning' => $request->warning]);
        return response()->json(['status' => 'success']);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\ParseArticleJob;
use App\Services\Admin\ParserService;
use App\Services\ArticleService;
use App\Services\TranslateService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ParserController extends Controller
{
    protected $excludedSetIds = [];

    public function __construct(
        protected ParserService $parserService,
        protected ArticleService $articleService
    ) {}

    public function parseArticle(TranslateService $translateService) // Instead of this method, DispatchParseArticleJob command is used
    {
        DB::beginTransaction();
        try {
            $result = $this->parserService->processRandomPage($this->excludedSetIds);

            if (isset($result['message'])) {
                echo $result['message'] . '<br><br>';

                if (isset($result['excludedSetId'])) {
                    $this->excludedSetIds[] = $result['excludedSetId'];
                    DB::commit();
                    return $this->parseArticle($translateService);
                }
            } else {
                $randomUrl = $result['randomUrl'];

                $articleData = $this->parserService->fetchArticleContent($randomUrl, $result['parsing_set_id'], $result['page']);

                if ($articleData['content'] === 'Content not found') {
                    DB::commit();
                    return $this->parseArticle($translateService);
                }

                if ($articleData['title'] === 'Error fetching the content') {
                    DB::commit();
                    return $this->parseArticle($translateService);
                }

                if ($this->parserService->shouldSkipArticle($articleData['content'], $result['parsing_set_id'], $randomUrl)) {
                    DB::commit();
                    return $this->parseArticle($translateService);
                }

                $articletWithTitleAndContent = $this->articleService->createArticle($articleData, $result['page'], $randomUrl);

                $article = $articletWithTitleAndContent[0];
                $title = $articletWithTitleAndContent[1];
                $content = $articletWithTitleAndContent[2];

                $translatedArticleContent = $this->articleService->translateAndFormat($title, $content, $translateService);

                $this->articleService->saveTranslation($article, $translatedArticleContent);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to parse article at URL: ' . ($randomUrl ?? 'URL not set') . '. Error: ' . $e->getMessage());
        }
    }

    public function dispatchParseArticleJob()
    {
        $translateApiKey = (new TranslateService(null))->checkTranslationLimits();

        if (!$translateApiKey) {
            return response()->json(['message' => 'No valid API key found'], 400);
        }

        ParseArticleJob::dispatch($translateApiKey);

        return response()->json(['message' => 'Parse article job has been dispatched.']);
    }
}

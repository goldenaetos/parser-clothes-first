<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PageRequest;
use App\Models\Admin\Category;
use App\Models\Admin\Page;
use App\Models\Admin\ParsingSet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        $pageTitle = 'Сторінки';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.pages.index', compact('pages', 'pageTitle', 'breadcrumbs'));
    }

    public function create()
    {
        $categories = Category::all();
        $parsingSets = ParsingSet::all();
        $pageTitle = 'Додати сторінку';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Сторінки', 'route' => route('pages.index')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.pages.create', compact('categories', 'parsingSets', 'pageTitle', 'breadcrumbs'));
    }

    public function store(PageRequest $request)
    {
        DB::beginTransaction();
        try {
            $validatedData = $request->validated();

            Page::create($validatedData);
            DB::commit();

            return redirect()->route('pages.index')->with('success', 'Page created successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to create page: ' . $e->getMessage());
            return redirect()->route('pages.index')->with('error', 'Failed to create page.');
        }
    }

    public function show(Page $page)
    {
        $pageTitle = 'Подробиці сторінки';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Сторінки', 'route' => route('pages.index')],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.pages.show', compact('page', 'pageTitle', 'breadcrumbs'));
    }

    public function edit(Page $page)
    {
        $categories = Category::all();
        $parsingSets = ParsingSet::all();
        $pageTitle = 'Редагувати сторінку';

        $breadcrumbs = [
            ['title' => 'Панель Адміністратора', 'route' => route('admin')],
            ['title' => 'Сторінки', 'route' => route('pages.index')],
            ['title' => $page->name, 'route' => route('pages.show', $page->id)],
            ['title' => $pageTitle, 'route' => null]
        ];

        return view('admin.pages.edit', compact('page', 'categories', 'parsingSets', 'pageTitle', 'breadcrumbs'));
    }

    public function update(PageRequest $request, Page $page)
    {
        DB::beginTransaction();
        try {
            $validatedData = $request->validated();

            $page->update($validatedData);
            DB::commit();

            return redirect()->route('pages.index')->with('success', 'Page updated successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to update page: ' . $e->getMessage());
            return redirect()->route('pages.index')->with('error', 'Failed to update page.');
        }
    }

    public function destroy(Page $page)
    {
        DB::beginTransaction();
        try {
            $page->delete();
            DB::commit();
            return redirect()->route('pages.index')->with('success', 'Page deleted successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Failed to delete page: ' . $e->getMessage());
            return redirect()->route('pages.index')->with('error', 'Failed to delete page.');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Services\ArticleService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    public function __construct(
        protected ArticleService $articleService,
    ) {}

    public function index()
    {
        $articles = Article::paginate(20);

        $articles->getCollection()->transform(function ($article) {
            $contentArray = json_decode($article->content, true);

            $text = '';
            foreach ($contentArray as $element) {
                if ($element['tag'] === 'p') {
                    $text .= strip_tags($element['content']) . ' ';
                }
            }

            $article->excerpt = Str::limit(trim($text), 125, '...');
            return $article;
        });

        return view('articles.index', compact('articles'));
    }

    public function show(Article $article)
    {
        return view('articles.show', compact('article'));
    }

    public function showRu(Article $article)
    {
        return view('articles.show_ru', compact('article'));
    }

    public function showUa(Article $article)
    {
        return view('articles.show_ua', compact('article'));
    }

    public function destroy(Article $article)
    {
        DB::beginTransaction();

        try {
            $deletedImages = [];
            $backupDirectory = storage_path('app/backup/');

            foreach ($article->images as $image) {
                $originalPath = $image->path;
                $backupPath = $backupDirectory . $originalPath;
                if (file_exists($originalPath)) {
                    copy($originalPath, $backupPath);
                    unlink($originalPath);
                }

                $deletedImages[] = [
                    'originalPath' => $originalPath,
                    'backupPath' => $backupPath
                ];
            }

            $article->delete();

            DB::commit();

            foreach ($deletedImages as $deletedImage) {
                if (file_exists($deletedImage['backupPath'])) {
                    unlink($deletedImage['backupPath']);
                }
            }

            return redirect()->route('articles.index')->with('success', 'Article with images deleted successfully');
        } catch (\Exception $e) {
            DB::rollBack();

            foreach ($deletedImages as $deletedImage) {
                if (file_exists($deletedImage['backupPath'])) {
                    copy($deletedImage['backupPath'], $deletedImage['originalPath']);
                    unlink($deletedImage['backupPath']); // Удаляем резервную копию после восстановления
                }
            }

            Log::error('Failed to delete article: ' . $e->getMessage());
            return redirect()->route('articles.index')->with('error', 'Article could not be deleted');
        }
    }
}

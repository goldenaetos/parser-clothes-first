<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'url' => 'required|string',
            'category_id' => 'required|exists:categories,id',
            'parsing_set_id' => 'required|exists:parsing_sets,id',
        ];
    }
}

<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ParsingSetRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $parsingSet = $this->route('parsingSet');

        return [
            'set_name' => ['required', 'string', Rule::unique('parsing_sets', 'set_name')->ignore($parsingSet?->id)->whereNull('deleted_at')],
            'site_address' => ['required', 'string'],
            'code_start' => ['required', 'string'],
            'code_end' => ['required', 'string'],
            'url_pattern' => ['required', 'string'],
            'url_exclusions' => ['nullable', 'array'],
            'url_exclusions.*' => ['string'],
            'keywords_to_skip_article' => ['nullable', 'array'],
            'keywords_to_skip_article.*' => ['string'],
            'header_classes_to_exclude' => ['nullable', 'array'],
            'header_classes_to_exclude.*' => ['string'],
            'paragraph_classes_to_exclude' => ['nullable', 'array'],
            'paragraph_classes_to_exclude.*' => ['string'],
            'span_classes_to_exclude' => ['nullable', 'array'],
            'span_classes_to_exclude.*' => ['string'],
            'ul_classes_to_exclude' => ['nullable', 'array'],
            'ul_classes_to_exclude.*' => ['string'],
            'all_elements_classes_to_exclude' => ['nullable', 'array'],
            'all_elements_classes_to_exclude.*' => ['string'],
            'all_element_classes_parts_to_exclude' => ['nullable', 'array'],
            'all_element_classes_parts_to_exclude.*' => ['string'],
            'image_classes_to_exclude' => ['nullable', 'array'],
            'image_classes_to_exclude.*' => ['string'],
            'path_to_image_contains_to_exclude' => ['nullable', 'array'],
            'path_to_image_contains_to_exclude.*' => ['string'],
            'stop_words' => ['nullable', 'array'],
            'stop_words.*' => ['string'],
            'excluded_full_tags' => ['nullable', 'array'],
            'excluded_full_tags.*' => ['string'],
            'excluded_tag_starts' => ['nullable', 'array'],
            'excluded_tag_starts.*' => ['string'],
            'pages' => ['nullable', 'array'],
            'pages.*.url' => ['required_with:pages.*.category_id', 'string'],
            'pages.*.category_id' => ['required_with:pages.*.url', 'exists:categories,id'],
        ];
    }
}

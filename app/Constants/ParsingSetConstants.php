<?php

namespace App\Constants;

class ParsingSetConstants
{
    public static function getElements()
    {
        return [
            ['name' => 'url_exclusions', 'description' => 'Части УРЛов, которые исключить'],
            ['name' => 'keywords_to_skip_article', 'description' => 'Слова, при встрече которых в статье пропустить статью'],
            ['name' => 'stop_words', 'description' => 'Стоп-слова, дальше которых не продолжать парсинг'],
            ['name' => 'header_classes_to_exclude', 'description' => 'Классы заголовков, которые исключить'],
            ['name' => 'paragraph_classes_to_exclude', 'description' => 'Классы абзацев, которые исключить'],
            ['name' => 'span_classes_to_exclude', 'description' => 'Классы спанов, которые исключить'],
            ['name' => 'ul_classes_to_exclude', 'description' => 'Классы списков (ul, ol), которые исключить'],
            ['name' => 'all_elements_classes_to_exclude', 'description' => 'Классы любых элементов, которые исключить'],
            ['name' => 'all_element_classes_parts_to_exclude', 'description' => 'Начальные символы классов любых элементов, которые исключить'],
            ['name' => 'image_classes_to_exclude', 'description' => 'Классы изображений, которые исключить'],
            ['name' => 'path_to_image_contains_to_exclude', 'description' => 'Исключить изображения, содержащие в адресе'],
            ['name' => 'excluded_full_tags', 'description' => 'Содержимое тегов, которые исключить при полном совпадении'],
            ['name' => 'excluded_tag_starts', 'description' => 'Начало содержимого тегов, которые исключить'],
        ];
    }

    public static function getStringElements()
    {
        return [
            ['label' => 'Название комплекта', 'name' => 'set_name', 'type' => 'text', 'required' => true],
            ['label' => 'Адрес сайта', 'name' => 'site_address', 'type' => 'text', 'required' => true],
            ['label' => 'Начало строки кода для определения УРЛа', 'name' => 'code_start', 'type' => 'text', 'required' => true],
            ['label' => 'Конец строки кода для определения УРЛа', 'name' => 'code_end', 'type' => 'text', 'required' => true],
            ['label' => 'Паттерн для определения УРЛов', 'name' => 'url_pattern', 'type' => 'text', 'required' => true],
        ];
    }

    public static function getSeparateElements()
    {
        return [
            ['label' => 'Страницы', 'name' => 'pages'],
        ];
    }
}

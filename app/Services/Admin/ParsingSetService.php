<?php

namespace App\Services\Admin;

use App\Models\Admin\ParsingSet;

class ParsingSetService
{
    public function getDuplicateSlugsWarning()
    {
        $duplicateSlugs = ParsingSet::select('slug')
            ->groupBy('slug')
            ->havingRaw('COUNT(*) > 1')
            ->pluck('slug');

        $warnings = [];
        if ($duplicateSlugs->isNotEmpty()) {
            foreach ($duplicateSlugs as $slug) {
                $duplicates = ParsingSet::where('slug', $slug)->get();
                $warnings[] = 'Внимание! Комплекты с ID: ' . $duplicates->pluck('id')->join(', ') . ' имеют одинаковые слаги. Исправьте ситуацию во избежание дальнейших ошибок при работе с ними!';
            }
        }

        return $warnings;
    }

    public function prepareData(array $data): array
    {
        $data['url_exclusions'] = $data['url_exclusions'] ?? [];
        $data['keywords_to_skip_article'] = $data['keywords_to_skip_article'] ?? [];
        $data['header_classes_to_exclude'] = $data['header_classes_to_exclude'] ?? [];
        $data['paragraph_classes_to_exclude'] = $data['paragraph_classes_to_exclude'] ?? [];
        $data['span_classes_to_exclude'] = $data['span_classes_to_exclude'] ?? [];
        $data['ul_classes_to_exclude'] = $data['ul_classes_to_exclude'] ?? [];
        $data['all_elements_classes_to_exclude'] = $data['all_elements_classes_to_exclude'] ?? [];
        $data['all_element_classes_parts_to_exclude'] = $data['all_element_classes_parts_to_exclude'] ?? [];
        $data['image_classes_to_exclude'] = $data['image_classes_to_exclude'] ?? [];
        $data['path_to_image_contains_to_exclude'] = $data['path_to_image_contains_to_exclude'] ?? [];
        $data['stop_words'] = $data['stop_words'] ?? [];
        $data['excluded_full_tags'] = $data['excluded_full_tags'] ?? [];
        $data['excluded_tag_starts'] = $data['excluded_tag_starts'] ?? [];

        return $data;
    }
}

<?php

namespace App\Services\Admin;

use App\Models\Admin\Page;
use App\Models\Admin\ParsingSet;
use App\Models\Admin\UsedUrl;
use App\Services\TranslateService;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ParserService
{
    public function __construct(
        protected TranslateService $translateService
    ) {}

    private function getRandomSetId(array $excludedSetIds = [])
    {
        $query = ParsingSet::query()->whereNull('deleted_at');

        if (!empty($excludedSetIds)) {
            $query->whereNotIn('id', $excludedSetIds);
        }

        $set = $query->inRandomOrder()->first();

        return $set ? $set->id : null;
    }

    private function getPages($parsingSetId)
    {
        $pages = Page::with('category')
            ->where('parsing_set_id', $parsingSetId)
            ->get();

        $result = $pages->map(function ($page) {
            return [
                'url' => $this->cleanUrl($page->url),
                'category_id' => $page->category_id,
                'category_name' => $page->category->name ?? 'Unknown'
            ];
        });

        return $result;
    }

    public function getRandomPage(array $excludedSetIds = [])
    {
        $randomSetId = $this->getRandomSetId($excludedSetIds);

        if (!$randomSetId) {
            return ['message' => 'No more sets available.'];
        }

        $pages = $this->getPages($randomSetId);

        if ($pages->isEmpty()) {
            return [
                'message' => 'No pages found for ParsingSetId: ' . $randomSetId,
                'excludedSetId' => $randomSetId
            ];
        }

        $randomPage = $pages->random();

        return [
            'parsing_set_id' => $randomSetId,
            'page' => $randomPage
        ];
    }

    public function fetchPageContent($url)
    {
        $cleanedUrl = $this->cleanUrl($url);
        $response = Http::withHeaders([
            'User-Agent' => $this->getRandomUserAgent()
        ])->get($cleanedUrl);

        return $response->body();
    }

    public function extractContentAndUrls($content, $codeStart, $codeEnd, $urlPattern)
    {
        $startPos = strpos($content, $codeStart);
        $endPos = strpos($content, $codeEnd, $startPos);

        if ($startPos === false || $endPos === false) {
            return [
                'extractedContent' => 'Content not found',
                'urls' => []
            ];
        }

        $extractedContent = substr($content, $startPos, $endPos - $startPos + strlen($codeEnd));
        preg_match_all($urlPattern, $extractedContent, $matches);
        $urls = array_map([$this, 'cleanUrl'], $matches[1]);

        return [
            'extractedContent' => $extractedContent,
            'urls' => $urls
        ];
    }

    public function processRandomPage($excludedSetIds)
    {
        $excludedUrls = [];

        while (true) {
            $result = $this->getRandomPage($excludedSetIds);

            if (isset($result['message'])) {
                return $result;
            }

            $pageUrl = $result['page']['url'];

            if (in_array($pageUrl, $excludedUrls)) {
                continue;
            }

            $pageContent = $this->fetchPageContent($pageUrl);

            $parsingSet = ParsingSet::find($result['parsing_set_id']);
            if (!$parsingSet) {
                return ['message' => 'Parsing set not found.'];
            }

            $codeStart = $parsingSet->code_start;
            $codeEnd = $parsingSet->code_end;
            $urlPattern = $parsingSet->url_pattern;
            $baseSiteUrl = $parsingSet->site_address;

            $extractedData = $this->extractContentAndUrls($pageContent, $codeStart, $codeEnd, $urlPattern);
            $urls = $this->removeEmptyUrls($extractedData['urls']);
            $urls = $this->removeDuplicateUrls($urls);
            $urls = $this->normalizeUrls($urls, $baseSiteUrl);
            $urls = $this->filterUsedUrls($urls);
            $urls = $this->filterExcludedUrls($urls, $parsingSet->url_exclusions);

            if (empty($urls)) {
                Log::info('No new URLs found for page: ' . $pageUrl);
                $excludedUrls[] = $pageUrl;
                continue;
            }

            $randomUrl = $urls[array_rand($urls)];
            $cleanedUrl = $this->cleanUrl($randomUrl);

            $this->saveUsedUrl($cleanedUrl);

            return [
                'parsing_set_id' => $result['parsing_set_id'],
                'page' => $result['page'],
                'extractedContent' => $extractedData['extractedContent'],
                'urls' => $urls,
                'randomUrl' => $cleanedUrl
            ];
        }
    }

    protected function removeDuplicateUrls(array $urls)
    {
        return array_unique($urls);
    }

    protected function removeEmptyUrls(array $urls)
    {
        return array_filter($urls, function ($url) {
            return !empty($url);
        });
    }

    protected function normalizeUrls(array $urls, $baseSiteUrl)
    {
        return array_map(function ($url) use ($baseSiteUrl) {
            if (!preg_match('/^https?:\/\//', $url)) {
                $url = rtrim($baseSiteUrl, '/') . '/' . ltrim($url, '/');
            }

            return preg_replace('/(?<!:)\/{2,}/', '/', $url);
        }, $urls);
    }

    private function filterExcludedUrls(array $urls, $urlExclusions)
    {
        return array_filter($urls, function ($url) use ($urlExclusions) {
            foreach ($urlExclusions as $urlExclusion) {
                if (stripos($url, $urlExclusion) !== false) {
                    return false;
                }
            }
            return true;
        });
    }

    private function filterUsedUrls(array $urls)
    {
        return array_filter($urls, function ($url) {
            $cleanedUrl = $this->cleanUrl($url);
            return !UsedUrl::where('url', $cleanedUrl)->exists();
        });
    }

    private function cleanUrl($url)
    {
        $urlParts = explode('?', $url, 2);
        return $urlParts[0];
    }

    private function saveUsedUrl($url)
    {
        UsedUrl::create(['url' => $url]);
    }

    private function getRandomUserAgent()
    {
        $userAgents = [
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, как Gecko) Chrome/58.0.3029.110 Safari/537.3',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, как Gecko) Version/14.0.1 Safari/605.1.15',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0',
            'Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, как Gecko) Chrome/126.0.0.0 Mobile Safari/537.36',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 17_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, как Gecko) Version/17.5 Mobile/15E148 Safari/604.1',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, как Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0',
            'Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, как Gecko) SamsungBrowser/25.0 Chrome/121.0.0.0 Mobile Safari/537.36',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 17_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, как Gecko) GSA/323.0.647062479 Mobile/15E148 Safari/604.1'
        ];

        return $userAgents[array_rand($userAgents)];
    }

    public function fetchArticleContent($url, $setId, $page)
    {
        try {
            $response = Http::withHeaders([
                'User-Agent' => $this->getRandomUserAgent()
            ])->get($url);

            if (!$response->successful()) {
                throw new \Exception("Failed to fetch the URL: " . $url);
            }

            $html = $response->body();
            $dom = new \DOMDocument();
            @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

            $xpath = new \DOMXPath($dom);

            $dom = $this->replaceDivsWithHeadingH3($dom, $xpath);

            $titleNode = $xpath->query('//title')->item(0);
            $title = $titleNode ? $this->processTitle($titleNode->textContent) : 'No title';

            $contentNode = $xpath->query('//article')->item(0);

            if (!$contentNode) {
                Log::error('Article tag not found (setId: ' . $setId . ', page: ' . $page['url'] . ', url: ' . $url . ').');
                $this->saveUsedUrl($url);
                return [
                    'content' => 'Content not found',
                ];
            }

            $articleHtml = $dom->saveHTML($contentNode);
            $articleHtml = $this->removeUnwantedElements($articleHtml);
            $articleHtml = $this->filterContent($articleHtml, $setId);
            $articleHtml = $this->processCaptionText($articleHtml);
            $articleHtml = $this->removeHashLinks($articleHtml);
            $articleHtml = $this->removeNonResultantTags($articleHtml);
            $articleHtml = $this->processImages($articleHtml);
            $articleHtml = $this->removeImagesWithPathContains($articleHtml, $setId);
            $articleHtml = $this->unwrapDivTags($articleHtml);
            $articleHtml = $this->unwrapSpanTags($articleHtml);
            $articleHtml = $this->addSpaceBetweenInlineTags($articleHtml);
            $articleHtml = $this->processTags($articleHtml);
            $articleHtml = $this->extractRelevantTags($articleHtml);
            $articleHtml = $this->removeDuplicateImagesWrappedInP($articleHtml);

            $contentJsonString = json_encode($articleHtml, JSON_UNESCAPED_UNICODE);

            return [
                'title' => $this->normalizeText($title),
                'content' => $this->normalizeText($contentJsonString)
            ];
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return [
                'title' => 'Error fetching the content',
                'content' => '',
            ];
        }
    }

    public function processCaptionText($html)
    {
        return preg_replace_callback('/<span[^>]*class="[^"]*caption__text[^"]*"[^>]*>(.*?)<\/span>/is', function ($matches) {
            $content = $matches[1];

            if (stripos($content, '<p>') !== false) {
                return preg_replace('/<p\b[^>]*>(.*?)<\/p>/is', '<p><sup>$1</sup></p>', $content);
            } else {
                return '<p><sup>' . $content . '</sup></p>';
            }
        }, $html);
    }

    public function removeDuplicateImagesWrappedInP($articleHtml)
    {
        $images = [];
        $newContent = [];

        foreach ($articleHtml as $element) {
            if ($element['tag'] === 'img') {
                $dom = new \DOMDocument();
                @$dom->loadHTML($element['content'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $imgTags = $dom->getElementsByTagName('img');
                if ($imgTags->length > 0) {
                    $src = $imgTags->item(0)->getAttribute('src');
                    $images[$src] = true;
                }
            }
        }

        foreach ($articleHtml as $element) {
            if ($element['tag'] === 'p') {
                $dom = new \DOMDocument();
                @$dom->loadHTML($element['content'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $imgTags = $dom->getElementsByTagName('img');
                if ($imgTags->length > 0) {
                    $src = $imgTags->item(0)->getAttribute('src');

                    if (isset($images[$src])) {
                        continue;
                    } else {
                        $images[$src] = true;
                        $newContent[] = $element;
                    }
                } else {
                    $newContent[] = $element;
                }
            } else {
                $newContent[] = $element;
            }
        }

        return $newContent;
    }

    protected function removeHashLinks($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $xpath = new \DOMXPath($dom);

        $links = $xpath->query('//a[starts-with(@href, "#")]');

        foreach ($links as $link) {
            $textNode = $dom->createTextNode($link->textContent);
            $link->parentNode->replaceChild($textNode, $link);
        }

        return $dom->saveHTML();
    }

    public function replaceDivsWithHeadingH3($dom, $xpath)
    {
        $divs = $xpath->query('//div[contains(@class, "heading-h3")]');

        foreach ($divs as $div) {
            $h3 = $dom->createElement('h3');

            while ($div->childNodes->length > 0) {
                $h3->appendChild($div->childNodes->item(0));
            }

            $div->parentNode->replaceChild($h3, $div);
        }

        return $dom;
    }

    private function addSpaceBetweenInlineTags($content)
    {
        $tagsPattern = 'em|i|b|strong|u|a|span|del';
        $pattern = '/(<\/(' . $tagsPattern . ')>)(<(' . $tagsPattern . '))/i';

        $replacement = '$1 $3';
        $processedContent = preg_replace($pattern, $replacement, $content);

        return $processedContent;
    }

    private function filterContent($html, $setId)
    {
        $html = $this->stopParsingAtStopWords($html, $setId);
        $html = $this->removeHeadingsWithClass($html, $setId);
        $html = $this->removeParagraphsWithClass($html, $setId);
        $html = $this->removeSpansWithClass($html, $setId);
        $html = $this->removeListsWithClass($html, $setId);
        $html = $this->removeElementsWithClass($html, $setId);
        $html = $this->removeElementsWithClassPart($html, $setId);
        $html = $this->removeImagesWithClass($html, $setId);
        $html = $this->removeFullTags($html, $setId);
        $html = $this->removeTagsStartingWith($html, $setId);

        return $html;
    }

    private function removeTagsStartingWith($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $tagStartsToExclude = $parsingSet->excluded_tag_starts;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($tagStartsToExclude as $tagStart) {
            $elements = $xpath->query("//*[starts-with(normalize-space(text()), '{$tagStart}')]");
            foreach ($elements as $element) {
                if ($this->isDeepestStartingTextElement($element, $tagStart)) {
                    $elementToDelete = $this->getDeletionCandidate($element);
                    if ($elementToDelete !== null) {
                        $elementToDelete->parentNode->removeChild($elementToDelete);
                    }
                }
            }
        }
        return $dom->saveHTML();
    }

    private function isDeepestStartingTextElement($element, $textStart) {
        $children = $element->childNodes;
        foreach ($children as $child) {
            if ($child instanceof \DOMElement && str_starts_with(trim($child->textContent), $textStart)) {
                return false;
            }
        }
        return true;
    }

    private function getDeletionCandidate($element) {
        $formattingTags = ['em', 'i', 'b', 'strong', 'u', 'a'];
        if (in_array(strtolower($element->tagName), $formattingTags)) {
            return $element->parentNode;
        }
        return $element;
    }

    private function removeFullTags($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $tagsToExclude = $parsingSet->excluded_full_tags;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($tagsToExclude as $tagContent) {
            $elements = $xpath->query("//*[normalize-space(.) = '{$tagContent}']");
            foreach ($elements as $element) {
                $element->parentNode->removeChild($element);
            }
        }

        return $dom->saveHTML();
    }

    private function removeImagesWithPathContains($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $pathsToExclude = $parsingSet->path_to_image_contains_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($pathsToExclude as $path) {
            $images = $xpath->query("//img[contains(@src, '{$path}')]");
            foreach ($images as $image) {
                $image->parentNode->removeChild($image);
            }
        }

        return $dom->saveHTML();
    }

    private function removeImagesWithClass($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $classesToExclude = $parsingSet->image_classes_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($classesToExclude as $class) {
            $images = $xpath->query("//img[contains(concat(' ', normalize-space(@class), ' '), ' {$class} ')]");
            foreach ($images as $image) {
                $image->parentNode->removeChild($image);
            }
        }

        return $dom->saveHTML();
    }

    private function removeElementsWithClassPart($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $classPartsToExclude = $parsingSet->all_element_classes_parts_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($classPartsToExclude as $classPart) {
            $elements = $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' {$classPart}')]");
            foreach ($elements as $element) {
                $classList = explode(' ', $element->getAttribute('class'));
                foreach ($classList as $class) {
                    if (strpos($class, $classPart) === 0) {
                        $element->parentNode->removeChild($element);
                        break;
                    }
                }
            }
        }

        return $dom->saveHTML();
    }

    private function removeElementsWithClass($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $classesToExclude = $parsingSet->all_elements_classes_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($classesToExclude as $class) {
            $elements = $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' {$class} ')]");
            foreach ($elements as $element) {
                $element->parentNode->removeChild($element);
            }
        }

        return $dom->saveHTML();
    }

    private function removeListsWithClass($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $classesToExclude = $parsingSet->ul_classes_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($classesToExclude as $class) {
            $lists = $xpath->query("//ul[contains(concat(' ', normalize-space(@class), ' '), ' {$class} ')] | //ol[contains(concat(' ', normalize-space(@class), ' '), ' {$class} ')]");
            foreach ($lists as $list) {
                $list->parentNode->removeChild($list);
            }
        }

        return $dom->saveHTML();
    }


    private function removeSpansWithClass($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $classesToExclude = $parsingSet->span_classes_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($classesToExclude as $class) {
            $spans = $xpath->query("//span[contains(concat(' ', normalize-space(@class), ' '), ' {$class} ')]");
            foreach ($spans as $span) {
                $span->parentNode->removeChild($span);
            }
        }

        return $dom->saveHTML();
    }

    private function removeParagraphsWithClass($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $classesToExclude = $parsingSet->paragraph_classes_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($classesToExclude as $class) {
            $paragraphs = $xpath->query("//p[contains(concat(' ', normalize-space(@class), ' '), ' {$class} ')]");
            foreach ($paragraphs as $paragraph) {
                $paragraph->parentNode->removeChild($paragraph);
            }
        }

        return $dom->saveHTML();
    }

    private function removeHeadingsWithClass($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $classesToExclude = $parsingSet->header_classes_to_exclude;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach (range(1, 6) as $i) {
            foreach ($classesToExclude as $class) {
                $headings = $xpath->query("//h{$i}[contains(concat(' ', normalize-space(@class), ' '), ' {$class} ')]");
                foreach ($headings as $heading) {
                    $heading->parentNode->removeChild($heading);
                }
            }
        }

        return $dom->saveHTML();
    }

    private function stopParsingAtStopWords($html, $setId)
    {
        $parsingSet = ParsingSet::find($setId);
        $stopWords = $parsingSet->stop_words;

        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        foreach ($stopWords as $stopWord) {
            $elements = $xpath->query("//*[starts-with(normalize-space(), '{$stopWord}')]");
            if ($elements->length > 0) {
                $firstElement = $elements->item(0);
                $this->removeFollowingContent($firstElement);
                break;
            }
        }

        return $dom->saveHTML();
    }

    private function removeFollowingContent($node)
    {
        $parent = $node->parentNode;
        while ($node->nextSibling) {
            $parent->removeChild($node->nextSibling);
        }

        $parent->removeChild($node);

        while ($parent && $parent->nodeName !== 'body') {
            $nextSibling = $parent->nextSibling;
            while ($nextSibling) {
                $nextParent = $nextSibling->parentNode;
                $next = $nextSibling->nextSibling;
                $nextParent->removeChild($nextSibling);
                $nextSibling = $next;
            }
            $parent = $parent->parentNode;
        }
    }

    private function getAbsoluteUrl($baseUrl, $relativeUrl)
    {
        if (parse_url($relativeUrl, PHP_URL_SCHEME) != '') {
            return $relativeUrl;
        }

        $base = rtrim(parse_url($baseUrl, PHP_URL_SCHEME) . '://' . parse_url($baseUrl, PHP_URL_HOST) . dirname(parse_url($baseUrl, PHP_URL_PATH)), '/') . '/';

        if ($relativeUrl[0] == '/') {
            $relativeUrl = ltrim($relativeUrl, '/');
        }

        $absUrl = $base . $relativeUrl;

        $absUrl = preg_replace('#(?<!:)//+#', '/', $absUrl);
        $absUrl = preg_replace('#/\./#', '/', $absUrl);
        $absUrl = preg_replace('#/[^/]+/\.\./#', '/', $absUrl);

        return $absUrl;
    }

    public function extractRelevantTags($articleHtml)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($articleHtml, 'HTML-ENTITIES', 'UTF-8'));

        $xpath = new \DOMXPath($dom);
        $tags = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'ul', 'ol', 'blockquote', 'img'];
        $result = [];

        $query = '//' . implode(' | //', $tags);
        $nodes = $xpath->query($query);

        foreach ($nodes as $node) {
            $nodeHtml = $dom->saveHTML($node);
            $result[] = [
                'tag' => $node->nodeName,
                'content' => $nodeHtml
            ];
        }

        return $result;
    }

    public function removeUnwantedElements($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);
        $tagsToRemove = ['time', 'button', 'video', 'iframe'];

        foreach ($tagsToRemove as $tag) {
            while (true) {
                $nodes = $xpath->query('//' . $tag);
                if ($nodes->length === 0) {
                    break;
                }
                foreach ($nodes as $node) {
                    $node->parentNode->removeChild($node);
                }
            }
        }

        return $dom->saveHTML();
    }

    public function unwrapSpanTags($html)
    {
        return preg_replace('/<span\b[^>]*>(.*?)<\/span>/i', '$1', $html);
    }

    public function unwrapDivTags($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        $divNodes = $xpath->query('//div');

        foreach ($divNodes as $div) {
            $fragment = $dom->createDocumentFragment();
            while ($div->childNodes->length > 0) {
                $fragment->appendChild($div->childNodes->item(0));
            }
            $div->parentNode->replaceChild($fragment, $div);
        }

        return $dom->saveHTML();
    }

    public function removeNonResultantTags($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $newDom = new \DOMDocument();
        $root = $newDom->createElement('div');
        $newDom->appendChild($root);

        $allowedTags = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'span', 'ul', 'ol', 'li', 'blockquote', 'picture', 'img', 'a', 'em', 'b', 'strong', 'i', 'u', 'sub', 'sup', 'del'];

        $this->recurseAndExtract($dom->documentElement, $root, $newDom, $allowedTags);

        return $newDom->saveHTML();
    }

    private function recurseAndExtract($node, $newParent, $newDom, $allowedTags)
    {
        foreach ($node->childNodes as $child) {
            if ($child instanceof \DOMElement) {
                if (in_array($child->tagName, $allowedTags)) {
                    $newElement = $newDom->createElement($child->tagName);
                    if ($child->tagName === 'img' || $child->tagName === 'a') {
                        foreach ($child->attributes as $attr) {
                            $newElement->setAttribute($attr->nodeName, $attr->nodeValue);
                        }
                    }
                    $this->recurseAndExtract($child, $newElement, $newDom, $allowedTags);
                    if ($newElement->hasChildNodes() || $newElement->tagName === 'img' || $newElement->tagName === 'a') {
                        $newParent->appendChild($newElement);
                    }
                } else {
                    $this->recurseAndExtract($child, $newParent, $newDom, $allowedTags);
                }
            } elseif ($child instanceof \DOMText && trim($child->nodeValue) !== '') {
                $newText = $newDom->createTextNode($child->nodeValue);
                $newParent->appendChild($newText);
            }
        }
    }

    public function processTags($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding("<div>{$html}</div>", 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        $handlers = [
            'a' => function($node) { $this->processAnchor($node); },
        ];

        foreach ($handlers as $tag => $handler) {
            $nodes = $xpath->query("//{$tag}");
            foreach ($nodes as $node) {
                $handler($node);
            }
        }

        $div = $dom->getElementsByTagName('div')->item(0);
        $newHtml = '';
        foreach ($div->childNodes as $child) {
            $newHtml .= $dom->saveHTML($child);
        }

        return $newHtml;
    }

    private function processAnchor($node)
    {
        $href = $node->getAttribute('href');
        $this->clearAttributes($node);
        $node->setAttribute('href', $href);
        $node->setAttribute('rel', 'nofollow');
    }

    private function clearAttributes($node)
    {
        while ($node->attributes->length > 0) {
            $node->removeAttribute($node->attributes->item(0)->name);
        }
    }

    private function processTitle($title) {
        $parts = explode(' | ', $title);
        $cleanTitle = $parts[0];

        return $this->normalizeText($cleanTitle);
    }

    public function normalizeText($text) {
        $text = str_replace('—', ' – ', $text);
        $text = str_replace(' - ', ' – ', $text);
        return preg_replace('/\s+/', ' ', $text);
    }

    public function shouldSkipArticle($content, $parsingSetId, $url)
    {
        $parsingSet = ParsingSet::find($parsingSetId);
        if (!$parsingSet || empty($parsingSet->keywords_to_skip_article)) {
            return false;
        }

        $keywords = $parsingSet->keywords_to_skip_article;
        foreach ($keywords as $keyword) {
            if ($this->filterContentByKeyword($content, $keyword)) {
                return true;
            }
        }

        return false;
    }

    public function filterContentByKeyword($content, $keyword)
    {
        if (strpos($content, $keyword) !== false) {
            return true;
        }

        return false;
    }

    public function processImages($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $xpath = new \DOMXPath($dom);

        $figures = $xpath->query('//figure');
        foreach ($figures as $figure) {
            $picture = $xpath->query('.//picture', $figure)->item(0);
            if ($picture) {
                $this->replaceWithLargestImage($dom, $xpath, $figure, $picture);
            }
        }

        $pictures = $xpath->query('//picture[not(ancestor::figure)]');
        foreach ($pictures as $picture) {
            $this->replaceWithLargestImage($dom, $xpath, $picture->parentNode, $picture);
        }

        $images = $xpath->query('//img[not(ancestor::figure) and not(ancestor::picture)]');
        foreach ($images as $img) {
            $this->processSingleImage($img);
        }

        return $dom->saveHTML();
    }

    private function replaceWithLargestImage($dom, $xpath, $parentNode, $picture)
    {
        $images = $xpath->query('.//img', $picture);
        if ($images->length === 0) {
            return;
        }

        $largestImage = null;
        $maxSize = 0;
        $alt = null;
        foreach ($images as $img) {
            $srcset = $img->getAttribute('srcset');
            $baseUrl = $img->getAttribute('src');
            if ($srcset) {
                $srcsetSizes = explode(',', $srcset);
                foreach ($srcsetSizes as $srcsetSize) {
                    $parts = explode(' ', trim($srcsetSize));
                    if (count($parts) == 2) {
                        list($src, $size) = $parts;
                        if (!filter_var($src, FILTER_VALIDATE_URL)) {
                            $src = $this->getAbsoluteUrl($baseUrl, $src);
                        }
                        $size = (int)$size;
                        if ($size > $maxSize) {
                            $maxSize = $size;
                            $largestImage = $src;
                            $alt = $img->getAttribute('alt');
                        }
                    }
                }
            } else {
                $size = getimagesize($baseUrl);
                if ($size) {
                    $imageSize = $size[0] * $size[1];
                    if ($imageSize > $maxSize) {
                        $maxSize = $imageSize;
                        $largestImage = $baseUrl;
                        $alt = $img->getAttribute('alt');
                    }
                }
            }
        }

        if ($largestImage) {
            $newImg = $dom->createElement('img');
            $newImg->setAttribute('src', $largestImage);
            if ($alt !== null && trim($alt) !== '') {
                $newImg->setAttribute('alt', $alt);
            }
            $parentNode->replaceChild($newImg, $picture);
        }
    }

    private function processSingleImage($img)
    {
        $src = $img->getAttribute('src');
        $alt = $img->getAttribute('alt');
        $this->clearAttributes($img);
        $img->setAttribute('src', $src);

        if ($alt !== null && trim($alt) !== '') {
            $img->setAttribute('alt', $alt);
        }
    }
}

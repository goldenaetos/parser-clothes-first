<?php

namespace App\Services\Admin;

use App\Models\Admin\Category;

class CategoryService
{
    public function getDuplicateSlugsWarning()
    {
        $duplicateSlugs = Category::select('slug')
            ->groupBy('slug')
            ->havingRaw('COUNT(*) > 1')
            ->pluck('slug');

        $warnings = [];
        if ($duplicateSlugs->isNotEmpty()) {
            foreach ($duplicateSlugs as $slug) {
                $duplicates = Category::where('slug', $slug)->get();
                $warnings[] = 'Внимание! Категории с ID: ' . $duplicates->pluck('id')->join(', ') . ' имеют одинаковые слаги. Исправьте ситуацию во избежание дальнейших ошибок при работе с ними!';
            }
        }

        return $warnings;
    }
}

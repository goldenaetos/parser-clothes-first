<?php

namespace App\Services;

use DeepL\Translator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class TranslateService
{
    public $translator;
    protected $apiKey;

    public function __construct($apiKey = null)
    {
        if ($apiKey) {
            $this->setApiKey($apiKey);
        }
    }

    public function translateHtmlContent(array $taggedContent, string $targetLang): array // used in ArticleService - in method: translateContent
    {
        foreach ($taggedContent as &$tagData) {
            if (!empty($tagData['content'])) {
                $translatedText = $this->translator->translateText($tagData['content'], 'en', $targetLang)->text;
                $tagData['content'] = $translatedText;
            }

            if (isset($tagData['alt']) && !empty($tagData['alt'])) {
                $translatedAlt = $this->translator->translateText($tagData['alt'], 'en', $targetLang)->text;
                $tagData['alt'] = $translatedAlt;
            }
        }

        return $taggedContent;
    }

    public function getUsage(): array
    {
        $client = new Client();

        try {
            $response = $client->get('https://api-free.deepl.com/v2/usage', [
                'headers' => [
                    'Authorization' => 'DeepL-Auth-Key ' . $this->apiKey,
                ],
            ]);

            $data = json_decode($response->getBody(), true);

            return [
                'character_count' => $data['character_count'] ?? 0,
                'character_limit' => $data['character_limit'] ?? 0,
            ];

        } catch (\Exception $e) {
            Log::error('API request failed: ' . $e->getMessage());
            return ['error' => $e->getMessage()];
        }
    }

    public function checkTranslationLimits(): string|bool
    {
        $apiKeys = [
            'DEEPL_API_KEY_1' => config('services.deepl.api_key_1'),
            'DEEPL_API_KEY_2' => config('services.deepl.api_key_2'),
            'DEEPL_API_KEY_3' => config('services.deepl.api_key_3'),
        ];

        $logs = [];

        foreach ($apiKeys as $keyName => $apiKey) {
            $this->setApiKey($apiKey);
            $usage = $this->getUsage();

            if (!isset($usage['error'])) {
                $remaining = $usage['character_limit'] - $usage['character_count'];

                if ($remaining > 50000) {
                    return $apiKey;
                }

                $logs[$keyName] = [
                    'used' => $usage['character_count'],
                    'limit' => $usage['character_limit'],
                ];
            } else {
                Log::error("Error using API Key {$keyName}: " . $usage['error']);
            }
        }

        if (!empty($logs)) {
            Log::warning('All DeepL API keys have insufficient character limit.', $logs);
        }

        return false;
    }

    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
        $baseUrl = str_contains($apiKey, ':fx') ? 'https://api-free.deepl.com' : 'https://api.deepl.com';
        $this->translator = new Translator($apiKey, ['base_uri' => $baseUrl]);
    }
}

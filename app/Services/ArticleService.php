<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Image;
use App\Services\Admin\ParserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ArticleService
{
    public function __construct(
        protected ImageService $imageService,
        protected ParserService $parserService
    ) {}

    public function createArticle(array $articleData, $commonPage, $url)
    {
        DB::beginTransaction();

        try {
            $savedImages = [];

            $article = Article::create([
                'slug' => $this->createSlugFromTitle($articleData['title']),
                'title' => $articleData['title'],
                'content' => '',
            ]);

            $processedContent = $this->handleImages($article->id, json_decode($articleData['content'], true), $savedImages);

            $article->update([
                'content' => $processedContent,
            ]);

            DB::commit();

            return [$article, $articleData['title'], $processedContent];
        } catch (\Exception $e) {
            DB::rollBack();

            foreach ($savedImages as $imagePath) {
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }

            Log::error('An error occurred when creating an article using a URL: ' . $commonPage . ', namely from a specific page: ' . $url . '. ' . $e->getMessage());

            throw $e;
        }
    }

    public function removeTrailingDotAfterClosingTag(array $translatedHtmlArray): array
    {
        foreach ($translatedHtmlArray as &$tagData) {
            if (substr($tagData['content'], -2, 1) === '>' && substr($tagData['content'], -1) === '.') {
                $tagData['content'] = rtrim($tagData['content'], '.');
            }
        }
        return $translatedHtmlArray;
    }

    protected function determineTypeFromPath($path)
    {
        $imageTypes = ['extra_small_preview', 'small_preview', 'preview'];

        foreach ($imageTypes as $type) {
            if (str_contains($path, $type)) {
                return $type;
            }
        }
    }

    protected function handleImages($articleId, $contentArray, &$savedImages)
    {
        $isFirstImage = true;
        foreach ($contentArray as &$block) {
            if ($block['tag'] === 'img') {
                $imageUrl = $this->extractImageUrl($block['content']);
                $savedImageData = $this->imageService->saveImage($imageUrl);

                $savedImages[] = public_path($savedImageData['path']);

                Image::create([
                    'article_id' => $articleId,
                    'type' => 'main',
                    'path' => $savedImageData['path'],
                ]);

                if ($isFirstImage) {
                    $previews = $this->imageService->createPreviews($savedImageData['image'], $savedImageData['name'], $savedImages);

                    foreach ($previews as $preview) {
                        $type = $this->determineTypeFromPath($preview['path']);
                        Image::create([
                            'article_id' => $articleId,
                            'type' => $type,
                            'path' => $preview['path'],
                        ]);

                        $savedImages[] = public_path($preview['path']);
                    }
                    $isFirstImage = false;
                }

                $block['content'] = str_replace($imageUrl, asset($savedImageData['path']), $block['content']);
            }
        }

        return json_encode($contentArray, JSON_UNESCAPED_UNICODE);
    }

    protected function createSlugFromTitle($title)
    {
        $slug = Str::slug($title);

        if (mb_strlen($slug) > 240) {
            $dashPos = mb_strrpos(mb_substr($slug, 200, 40), '-');

            if ($dashPos !== false) {
                $slug = mb_substr($slug, 0, 200 + $dashPos);
            } else {
                $slug = mb_substr($slug, 0, 240);
            }
        }

        if (mb_substr($slug, -1) === '-') {
            $slug = mb_substr($slug, 0, -1);
        }

        $date = now()->format('-Y-m-d');

        return $slug . $date;
    }

    protected function extractImageUrl($imgTag)
    {
        preg_match('/src="([^"]+)"/', $imgTag, $matches);
        return $matches[1];
    }

    public function translateAndFormat($title, $content, TranslateService $translateService)
    {
        $titleRu = $this->translateTitle($title, $translateService, 'ru');
        $titleUk = $this->translateTitle($title, $translateService, 'uk');

        $formattedTitleRu = $this->parserService->normalizeText($titleRu);
        $formattedTitleUk = $this->parserService->normalizeText($titleUk);

        $contentRu = $this->translateContent($content, $translateService, 'ru');
        $contentUk = $this->translateContent($content, $translateService, 'uk');

        $formattedContentRu = $this->formatContent($contentRu);
        $formattedContentUk = $this->formatContent($contentUk);

        $contentJsonStringRu = json_encode($formattedContentRu, JSON_UNESCAPED_UNICODE);
        $contentJsonStringUk = json_encode($formattedContentUk, JSON_UNESCAPED_UNICODE);

        return [
            'title_ru' => $formattedTitleRu,
            'title_uk' => $formattedTitleUk,
            'content_ru' => $contentJsonStringRu,
            'content_uk' => $contentJsonStringUk
        ];
    }

    private function translateTitle($title, $translateService, $language)
    {
        return $translateService->translator->translateText($title, 'en', $language)->text;
    }

    private function translateContent($content, $translateService, $language)
    {
        return $translateService->translateHtmlContent(json_decode($content, true), $language);
    }

    private function formatContent($content)
    {
        $content = $this->removeTrailingDotAfterClosingTag($content);
        $content = $this->removeEmptyParagraphs($content);
        $content = $this->removeLeadingAngleBracket($content);
        $content = $this->removePTagsAndWrapContent($content);
        $content = $this->replaceDashesAndSpaces($content);

        return $content;
    }

    public function replaceDashesAndSpaces(array $translatedHtmlArray): array
    {
        foreach ($translatedHtmlArray as &$tagData) {
            if (isset($tagData['content'])) {
                $tagData['content'] = $this->parserService->normalizeText($tagData['content']);
            }
        }
        return $translatedHtmlArray;
    }

    public function removePTagsAndWrapContent(array $translatedHtmlArray): array
    {
        foreach ($translatedHtmlArray as &$tagData) {
            if (isset($tagData['content'])) {
                $cleanedContent = str_replace(['<p>', '</p>'], '', $tagData['content']);

                if ($tagData['tag'] === 'p') {
                    $tagData['content'] = '<p>' . $cleanedContent . '</p>';
                } else {
                    $tagData['content'] = $cleanedContent;
                }
            }
        }
        return $translatedHtmlArray;
    }

    public function removeEmptyParagraphs(array $translatedHtmlArray): array
    {
        foreach ($translatedHtmlArray as &$tagData) {
            if ($tagData['tag'] === 'p' && trim($tagData['content']) === '') {
                $tagData = null;
            }
        }

        return array_filter($translatedHtmlArray);
    }

    public function removeLeadingAngleBracket(array $translatedHtmlArray): array
    {
        foreach ($translatedHtmlArray as &$tagData) {
            if ($tagData['tag'] === 'p' && isset($tagData['content']) && substr(trim($tagData['content']), 0, 1) === '<') {
                $tagData['content'] = ltrim($tagData['content'], '<');
            }

            if ($tagData['tag'] === 'p' && isset($tagData['content'])) {
                $tagData['content'] = preg_replace('/^p>/i', '', $tagData['content']);
            }
        }
        return $translatedHtmlArray;
    }

    public function saveTranslation($article, $translatedContent)
    {
        $article->update($translatedContent);
    }
}


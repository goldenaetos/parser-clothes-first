<?php

namespace App\Services;

use Illuminate\Support\Str;
use Intervention\Image\Encoders\WebpEncoder;
use Intervention\Image\ImageManager;

class ImageService
{
    protected function convertToWebP($image)
    {
        $webpEncoder = new WebpEncoder(100);
        return $image->encode($webpEncoder);
    }

    public function saveImage($url)
    {
        $imageContent = file_get_contents($url);
        $manager = ImageManager::gd();
        $image = $manager->read($imageContent);

        $imageInfo = getimagesizefromstring($imageContent);
        $mime = $imageInfo['mime'];
        $extension = $this->getExtensionFromMime($mime);

        if (!$extension) {
            throw new \Exception('Unsupported image type');
        }

        if ($extension === 'jpeg' || $extension === 'jpg' || $extension === 'png') {
            $image = $this->convertToWebP($image);
            $tempWebPPath = tempnam(sys_get_temp_dir(), 'webp_');
            $image->save($tempWebPPath);

            $image = $manager->read($tempWebPPath);
            $extension = 'webp';
            unlink($tempWebPPath);
        }

        $imageName = Str::random(20) . '.' . $extension;
        $mainImagePath = 'images/main/' . $imageName;
        $tempFilePath = tempnam(sys_get_temp_dir(), 'image_');

        $image->save($tempFilePath);

        $compressedFilePath = $this->compressImage($tempFilePath, $extension, 150000, $image);

        file_put_contents(public_path($mainImagePath), file_get_contents($compressedFilePath));
        unlink($tempFilePath);
        unlink($compressedFilePath);

        return [
            'path' => $mainImagePath,
            'url' => asset($mainImagePath),
            'name' => $imageName,
            'image' => $image
        ];
    }

    public function createPreviews($image, $imageName, &$savedImages)
    {
        $previews = [];
        $previews[] = $this->createPreview($image, $imageName, 'previews', [540, 360], 35000, $savedImages);
        $previews[] = $this->createPreview($image, $imageName, 'small_previews', [100, 100], 15000, $savedImages);
        $previews[] = $this->createPreview($image, $imageName, 'extra_small_previews', [80, 80], 5000, $savedImages);

        return $previews;
    }

    protected function createPreview($image, $imageName, $folder, $dimensions, $maxSize, &$savedImages)
    {
        $previewPath = 'images/' . $folder . '/' . $imageName;

        $image->resize($dimensions[0], $dimensions[1], function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $image->crop($dimensions[0], $dimensions[1]);

        $mime = $image->exif()->get('FILE.MimeType');
        $extension = $this->getExtensionFromMime($mime) ?: 'webp';

        $tempFilePath = tempnam(sys_get_temp_dir(), 'image_');

        $image->save($tempFilePath);

        $compressedFilePath = $this->compressImage($tempFilePath, $extension, $maxSize, $image);

        file_put_contents(public_path($previewPath), file_get_contents($compressedFilePath));
        unlink($tempFilePath);
        unlink($compressedFilePath);

        $savedImages[] = public_path($previewPath);

        return [
            'path' => $previewPath,
            'url' => asset($previewPath)
        ];
    }

    protected function compressImage($tempFilePath, $extension, $maxSize, $image)
    {
        $imagePath = $tempFilePath . '.' . $extension;
        $image->save($imagePath);

        $currentFileSize = filesize($imagePath);
        if ($currentFileSize <= $maxSize) {
            return $imagePath;
        }

        $imagick = new \Imagick($imagePath);
        $imagick->stripImage();

        switch ($extension) {
            case 'jpg':
            case 'jpeg':
            case 'webp':
                $quality = 90;
                do {
                    $imagick->setImageCompressionQuality($quality);
                    $imagick->writeImage($imagePath);
                    $currentFileSize = filesize($imagePath);
                    $quality -= 10;
                } while ($currentFileSize > $maxSize && $quality > 10);
                break;
            case 'gif':
                $imagick->writeImage($imagePath);
                break;
            default:
                throw new \Exception("Unsupported image type");
        }

        $imagick->destroy();

        return $imagePath;
    }

    protected function getExtensionFromMime($mime)
    {
        return match ($mime) {
            'image/jpeg' => 'jpeg',
            'image/jpg' => 'jpg',
            'image/png' => 'png',
            'image/gif' => 'gif',
            'image/webp' => 'webp',
            default => null,
        };
    }
}

<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = [
        'parsing_set_id',
        'url',
        'category_id',
    ];

    public function parsingSet()
    {
        return $this->belongsTo(ParsingSet::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}

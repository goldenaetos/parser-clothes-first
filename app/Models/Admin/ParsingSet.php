<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParsingSet extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'set_name',
        'slug',
        'site_address',
        'code_start',
        'code_end',
        'url_pattern',
        'url_exclusions',
        'keywords_to_skip_article',
        'header_classes_to_exclude',
        'paragraph_classes_to_exclude',
        'span_classes_to_exclude',
        'ul_classes_to_exclude',
        'all_elements_classes_to_exclude',
        'all_element_classes_parts_to_exclude',
        'image_classes_to_exclude',
        'path_to_image_contains_to_exclude',
        'stop_words',
        'excluded_full_tags',
        'excluded_tag_starts',
    ];

    protected $casts = [
        'url_exclusions' => 'array',
        'keywords_to_skip_article' => 'array',
        'header_classes_to_exclude' => 'array',
        'paragraph_classes_to_exclude' => 'array',
        'span_classes_to_exclude' => 'array',
        'ul_classes_to_exclude' => 'array',
        'all_elements_classes_to_exclude' => 'array',
        'all_element_classes_parts_to_exclude' => 'array',
        'image_classes_to_exclude' => 'array',
        'path_to_image_contains_to_exclude' => 'array',
        'stop_words' => 'array',
        'excluded_full_tags' => 'array',
        'excluded_tag_starts' => 'array',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function pages()
    {
        return $this->hasMany(Page::class);
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\ParserController;

class DispatchParseArticleJob extends Command
{
    protected $signature = 'parse:article';
    protected $description = 'Dispatch the ParseArticleJob to parse article';

    public function __construct(protected ParserController $parserController)
    {
        parent::__construct();
    }

    public function handle()
    {
        $response = $this->parserController->dispatchParseArticleJob();

        if ($response->status() === 200) {
            $this->info('Parse article job has been dispatched.');
        } else {
            $this->error('Error: ' . $response->getContent());
        }
    }
}

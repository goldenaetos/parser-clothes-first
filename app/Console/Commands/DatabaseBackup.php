<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseBackup extends Command
{
    protected $signature = 'db:backup';

    protected $description = 'Create a backup of the database';

    public function handle()
    {
        $date = date('Y_m_d_His');
        $fileName = "localhost_{$date}.sql";
        $filePath = storage_path("backups/{$fileName}");

        $command = sprintf(
            'mysqldump --user=%s --password=%s --host=%s %s > %s',
            escapeshellarg(config('database.connections.mysql.username')),
            escapeshellarg(config('database.connections.mysql.password')),
            escapeshellarg(config('database.connections.mysql.host')),
            escapeshellarg(config('database.connections.mysql.database')),
            escapeshellarg($filePath)
        );

        $returnVar = null;
        $output = null;
        exec($command, $output, $returnVar);

        if ($returnVar !== 0) {
            $this->error('Database backup failed.');
            return 1;
        }

        $this->info('Database backup created successfully.');
        return 0;
    }
}

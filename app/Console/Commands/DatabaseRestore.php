<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseRestore extends Command
{
    protected $signature = 'db:restore {backupFile}';

    protected $description = 'Restore the database from a backup file';

    public function handle()
    {
        $backupFile = $this->argument('backupFile');
        $filePath = storage_path("backups/{$backupFile}");

        if (!file_exists($filePath)) {
            $this->error("Backup file does not exist: {$filePath}");
            return 1;
        }

        $command = sprintf(
            'mysql --user=%s --password=%s --host=%s %s < %s',
            escapeshellarg(config('database.connections.mysql.username')),
            escapeshellarg(config('database.connections.mysql.password')),
            escapeshellarg(config('database.connections.mysql.host')),
            escapeshellarg(config('database.connections.mysql.database')),
            escapeshellarg($filePath)
        );

        $returnVar = null;
        $output = null;
        exec($command, $output, $returnVar);

        if ($returnVar !== 0) {
            $this->error('Database restore failed.');
            return 1;
        }

        $this->info('Database restored successfully.');
        return 0;
    }
}

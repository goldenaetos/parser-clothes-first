/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/admin/categories/edit.js":
/*!***********************************************!*\
  !*** ./resources/js/admin/categories/edit.js ***!
  \***********************************************/
/***/ (() => {

eval("$(document).ready(function () {\n  var $input = $('#name');\n  $input.focus();\n  var value = $input.val();\n  $input.val('');\n  $input.val(value);\n  var originalName = value;\n  $('#categoryForm').submit(function (event) {\n    var currentName = $input.val();\n    if (currentName === originalName) {\n      event.preventDefault();\n      $.ajax({\n        url: '/admin/categories/set-warning-message',\n        method: 'POST',\n        contentType: 'application/json',\n        headers: {\n          'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')\n        },\n        data: JSON.stringify({\n          warning: 'Category name has not been changed.'\n        }),\n        success: function success() {\n          window.location.href = '/admin/categories';\n        }\n      });\n    }\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsIiRpbnB1dCIsImZvY3VzIiwidmFsdWUiLCJ2YWwiLCJvcmlnaW5hbE5hbWUiLCJzdWJtaXQiLCJldmVudCIsImN1cnJlbnROYW1lIiwicHJldmVudERlZmF1bHQiLCJhamF4IiwidXJsIiwibWV0aG9kIiwiY29udGVudFR5cGUiLCJoZWFkZXJzIiwiYXR0ciIsImRhdGEiLCJKU09OIiwic3RyaW5naWZ5Iiwid2FybmluZyIsInN1Y2Nlc3MiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsImhyZWYiXSwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2FkbWluL2NhdGVnb3JpZXMvZWRpdC5qcz80MDMwIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuICAgIGNvbnN0ICRpbnB1dCA9ICQoJyNuYW1lJyk7XG4gICAgJGlucHV0LmZvY3VzKCk7XG4gICAgY29uc3QgdmFsdWUgPSAkaW5wdXQudmFsKCk7XG4gICAgJGlucHV0LnZhbCgnJyk7XG4gICAgJGlucHV0LnZhbCh2YWx1ZSk7XG5cbiAgICBjb25zdCBvcmlnaW5hbE5hbWUgPSB2YWx1ZTtcblxuICAgICQoJyNjYXRlZ29yeUZvcm0nKS5zdWJtaXQoZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgY29uc3QgY3VycmVudE5hbWUgPSAkaW5wdXQudmFsKCk7XG5cbiAgICAgICAgaWYgKGN1cnJlbnROYW1lID09PSBvcmlnaW5hbE5hbWUpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgICAgIHVybDogJy9hZG1pbi9jYXRlZ29yaWVzL3NldC13YXJuaW5nLW1lc3NhZ2UnLFxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAnWC1DU1JGLVRPS0VOJzogJCgnbWV0YVtuYW1lPVwiY3NyZi10b2tlblwiXScpLmF0dHIoJ2NvbnRlbnQnKVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoeyB3YXJuaW5nOiAnQ2F0ZWdvcnkgbmFtZSBoYXMgbm90IGJlZW4gY2hhbmdlZC4nIH0pLFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvYWRtaW4vY2F0ZWdvcmllcyc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9KTtcbn0pO1xuIl0sIm1hcHBpbmdzIjoiQUFBQUEsQ0FBQyxDQUFDQyxRQUFRLENBQUMsQ0FBQ0MsS0FBSyxDQUFDLFlBQVc7RUFDekIsSUFBTUMsTUFBTSxHQUFHSCxDQUFDLENBQUMsT0FBTyxDQUFDO0VBQ3pCRyxNQUFNLENBQUNDLEtBQUssQ0FBQyxDQUFDO0VBQ2QsSUFBTUMsS0FBSyxHQUFHRixNQUFNLENBQUNHLEdBQUcsQ0FBQyxDQUFDO0VBQzFCSCxNQUFNLENBQUNHLEdBQUcsQ0FBQyxFQUFFLENBQUM7RUFDZEgsTUFBTSxDQUFDRyxHQUFHLENBQUNELEtBQUssQ0FBQztFQUVqQixJQUFNRSxZQUFZLEdBQUdGLEtBQUs7RUFFMUJMLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQ1EsTUFBTSxDQUFDLFVBQVNDLEtBQUssRUFBRTtJQUN0QyxJQUFNQyxXQUFXLEdBQUdQLE1BQU0sQ0FBQ0csR0FBRyxDQUFDLENBQUM7SUFFaEMsSUFBSUksV0FBVyxLQUFLSCxZQUFZLEVBQUU7TUFDOUJFLEtBQUssQ0FBQ0UsY0FBYyxDQUFDLENBQUM7TUFDdEJYLENBQUMsQ0FBQ1ksSUFBSSxDQUFDO1FBQ0hDLEdBQUcsRUFBRSx1Q0FBdUM7UUFDNUNDLE1BQU0sRUFBRSxNQUFNO1FBQ2RDLFdBQVcsRUFBRSxrQkFBa0I7UUFDL0JDLE9BQU8sRUFBRTtVQUNMLGNBQWMsRUFBRWhCLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDaUIsSUFBSSxDQUFDLFNBQVM7UUFDL0QsQ0FBQztRQUNEQyxJQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBUyxDQUFDO1VBQUVDLE9BQU8sRUFBRTtRQUFzQyxDQUFDLENBQUM7UUFDeEVDLE9BQU8sRUFBRSxTQUFBQSxRQUFBLEVBQVc7VUFDaEJDLE1BQU0sQ0FBQ0MsUUFBUSxDQUFDQyxJQUFJLEdBQUcsbUJBQW1CO1FBQzlDO01BQ0osQ0FBQyxDQUFDO0lBQ047RUFDSixDQUFDLENBQUM7QUFDTixDQUFDLENBQUMiLCJpZ25vcmVMaXN0IjpbXSwiZmlsZSI6Ii4vcmVzb3VyY2VzL2pzL2FkbWluL2NhdGVnb3JpZXMvZWRpdC5qcyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/admin/categories/edit.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/admin/categories/edit.js"]();
/******/ 	
/******/ })()
;
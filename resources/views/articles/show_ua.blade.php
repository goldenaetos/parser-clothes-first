<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $article->title_uk }}</title>
</head>
<body>
<div>
    @foreach (json_decode($article->content_uk, true) as $content)
        @if (isset($content['tag']) && isset($content['content']))
            @php
                // Декодирование HTML-сущностей для корректного отображения
                $decodedContent = html_entity_decode($content['content']);
                // Вывод содержимого с учётом тегов
                echo $decodedContent;
            @endphp
        @endif
    @endforeach
</div>

<!-- Форма для удаления статьи -->
<form action="{{ route('articles.destroy', $article) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this article?');">
    @csrf
    @method('DELETE')
    <button type="submit">Delete Article</button>
</form>

</body>
</html>

@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="padding-left: 0; padding-right: 0;">
                <div class="box">
                    <div class="box-body">
                        <table id="adminpanel_table" class="table table-bordered table-striped table-adminpanel">
                            <thead>
                            <tr>
                                <th class="center">№</th>
                                <th>Field</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stringElements as $index => $field)
                                <tr>
                                    <td class="number-in-admin-table">{{ $index + 1 }}.</td>
                                    <td><strong>{{ $field['label'] }}</strong></td>
                                    <td class="pr-0">{{ $parsingSet->{$field['name']} }}</td>
                                </tr>
                            @endforeach

                            @php
                                $pageIndex = count($stringElements) + 1;
                            @endphp
                            <tr>
                                <td class="number-in-admin-table">{{ $pageIndex }}.</td>
                                <td><strong>Страницы</strong></td>
                                <td class="pr-0">
                                    <ul>
                                        @foreach($parsingSet->pages as $page)
                                            <li style="margin-left: -20px;margin-bottom: 10px;">
                                                <strong>URL:</strong> {{ $page->url }}<br>
                                                <strong>категорія:</strong> {{ $page->category->name ?? '[–]' }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>

                            @foreach($elements as $index => $element)
                                <tr>
                                    <td class="number-in-admin-table">{{ $index + $pageIndex + 1 }}.</td>
                                    <td><strong>{{ $element['description'] }}</strong></td>
                                    <td class="pr-0">
                                        <ul class="list-unstyled">
                                            @foreach($parsingSet->{$element['name']} as $item)
                                                <li>{{ $item }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach

                            @foreach(array_slice($separateElements, 1) as $index => $element)
                                <tr>
                                    <td class="number-in-admin-table">{{ $index + $pageIndex + count($elements) + 1 }}.</td>
                                    <td><strong>{{ $element['label'] }}</strong></td>
                                    <td class="pr-0">{{ $parsingSet->{$element['name']} ?? '' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div>
                            <a href="{{ route('sets.edit', $parsingSet->slug) }}" class="btn btn-warning" style="width: 100px; margin-right: 5px;">Редагувати</a>
                            <button style="width: 100px" type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop" data-url="{{ route('sets.destroy', $parsingSet->slug) }}">Видалити</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.components.set-delete-modal')

@endsection

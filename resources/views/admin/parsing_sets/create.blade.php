@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="padding-left: 0; padding-right: 0;">
                <div class="box">
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger" id="error-message">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('sets.store') }}" method="POST">
                            @csrf
                            <table id="adminpanel_table" class="table table-bordered table-striped table-adminpanel">
                                <thead>
                                <tr>
                                    <th class="center">№</th>
                                    <th>Field</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stringElements as $index => $field)
                                    <tr>
                                        <td class="number-in-admin-table">{{ $index + 1 }}.</td>
                                        <td><label for="{{ $field['name'] }}">{{ $field['label'] }}</label></td>
                                        <td class="pr-0">
                                            <input style="width: 270px;" type="{{ $field['type'] }}" class="form-control" id="{{ $field['name'] }}" name="{{ $field['name'] }}" value="{{ old($field['name']) }}" @if($field['required']) required @endif>
                                        </td>
                                    </tr>
                                @endforeach

                                @php
                                    $pageIndex = count($stringElements) + 1;
                                @endphp
                                <tr>
                                    <td class="number-in-admin-table">{{ $pageIndex }}.</td>
                                    <td><label for="pages">Страницы</label></td>
                                    <td class="input-with-button-td">
                                        <div class="input-group input-with-button-div">
                                            <input type="text" class="form-control input-near-button" id="page_url_input">
                                            <select class="form-control input-near-button" id="page_category_input">
                                                <option value="">Выберите категорию</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                            <button type="button" class="btn btn-primary button-near-input" id="add_page">Додати</button>
                                        </div>
                                        <ul id="pages_list" class="list-unstyled">
                                            @foreach(old('pages', []) as $index => $page)
                                                <li>
                                                    {{ $page['url'] }} - <span class="category-name" data-category-id="{{ $page['category_id'] }}"></span>
                                                    <span class="glyphicon glyphicon-remove-circle red" style="cursor: pointer;" onclick="this.parentElement.remove();"></span>
                                                    <input type="hidden" name="pages[{{ $index }}][url]" value="{{ $page['url'] }}">
                                                    <input type="hidden" name="pages[{{ $index }}][category_id]" value="{{ $page['category_id'] }}">
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>

                                @foreach($elements as $index => $element)
                                    <tr>
                                        <td class="number-in-admin-table">{{ $index + $pageIndex + 1 }}.</td>
                                        <td><label for="{{ $element['name'] }}">{{ $element['description'] }}</label></td>
                                        <td class="input-with-button-td">
                                            <div class="input-group input-with-button-div">
                                                <input type="text" class="form-control input-near-button" id="{{ $element['name'] }}_input">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-primary button-near-input" id="add_{{ $element['name'] }}">Додати</button>
                                                </div>
                                            </div>
                                            <ul id="{{ $element['name'] }}_list" class="list-unstyled">
                                                @foreach((array) old($element['name'], []) as $value)
                                                    <li>
                                                        {{ $value }}
                                                        <span class="glyphicon glyphicon-remove-circle red" style="cursor: pointer;" onclick="this.parentElement.remove();"></span>
                                                        <input type="hidden" name="{{ $element['name'] }}[]" value="{{ $value }}">
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach

                                @foreach(array_slice($separateElements, 1) as $index => $element)
                                    <tr>
                                        <td class="number-in-admin-table">{{ $index + $pageIndex + count($elements) + 1 }}.</td>
                                        <td><label for="{{ $element['name'] }}">{{ $element['label'] }}</label></td>
                                        <td class="pr-0">
                                            <input style="width: 270px;" type="{{ $element['type'] }}" class="form-control" id="{{ $element['name'] }}" name="{{ $element['name'] }}" value="{{ old($element['name']) }}">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <button style="width: 100px;" type="submit" class="btn btn-primary">Зберегти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('page-scripts')
    <script>
        window.elements = {!! json_encode($elements, JSON_UNESCAPED_UNICODE) !!};
    </script>
@endsection

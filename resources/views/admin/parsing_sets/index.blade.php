@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">
                    @if(session('success'))
                        <div class="alert alert-success" style="font-size: 1.2em;">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger" style="font-size: 1.2em;">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if(session('warning'))
                        <div class="alert alert-warning" style="font-size: 1.2em;">
                            {{ session('warning') }}
                        </div>
                        @php
                            session()->forget('warning');
                        @endphp
                    @endif

                    @if(isset($warnings) && !empty($warnings))
                        @foreach($warnings as $warning)
                            <div class="alert alert-warning" style="font-size: 1.2em;">
                                {{ $warning }}
                            </div>
                        @endforeach
                    @endif

                    <table class="table table-bordered table-hover" style="font-size: 1.2em;">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Назва комплекту</th>
                            <th style="width: 220px;">Що зробити</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($parsingSets as $parsingSet)
                            <tr>
                                <td>{{ $parsingSet->id }}</td>
                                <td><a href="{{ route('sets.show', $parsingSet->slug) }}">{{ $parsingSet->set_name }}</a></td>
                                <td>
                                    <a style="width: 100px" href="{{ route('sets.edit', $parsingSet->slug) }}" class="btn btn-warning">Редагувати</a>
                                    <button style="width: 100px" type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop" data-url="{{ route('sets.destroy', $parsingSet->slug) }}">Видалити</button>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><a style="width: 205px" href="{{ route('sets.create') }}" class="btn btn-primary pull-right">Додати комплект</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    @include('admin.components.set-delete-modal')

@endsection

@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">
                    @if(session('error'))
                        <div class="alert alert-danger" style="font-size: 1.2em;">
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>Категорії</h3>
                                    <p>Управління категоріями</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="{{ route('categories.index') }}" class="small-box-footer">Детальніше <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>Сторінки</h3>
                                    <p>Управління сторінками</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{ route('pages.index') }}" class="small-box-footer">Детальніше <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>Користувачі</h3>
                                    <p>Управління користувачами</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="#" class="small-box-footer">Детальніше <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>Налаштування</h3>
                                    <p>Конфігурація системи</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="#" class="small-box-footer">Детальніше <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

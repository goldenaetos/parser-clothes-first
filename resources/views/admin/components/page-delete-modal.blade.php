<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Видалення сторінки</h5>
                <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel">Видалити сторінку</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body center">
                                Ви впевнені, що бажаєте видалити сторінку?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info" style="background-color: #3c8dbc;" data-dismiss="modal">Закрити</button>
                                <button type="button" class="btn btn-danger delete-confirm">Видалити</button>
                            </div>
                            <form id="delete-form" action="" method="POST" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                </div>

                <script>
                    $('#staticBackdrop').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget);
                        var url = button.data('url');
                        var modal = $(this);
                        modal.find('#delete-form').attr('action', url);
                    });

                    $('.delete-confirm').on('click', function() {
                        $('#delete-form').submit();
                    });
                </script>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body center">
                Ви впевнені, що бажаєте видалити сторінку?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" style="background-color: #3c8dbc;" data-dismiss="modal">Закрити</button>
                <button type="button" class="btn btn-danger delete-confirm">Видалити</button>
            </div>
            <form id="delete-form" action="" method="POST" style="display: none;">
                @csrf
                @method('DELETE')
            </form>
        </div>
    </div>
</div>

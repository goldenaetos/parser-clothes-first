<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Видалення категорії</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body center">
                Ви впевнені, що бажаєте видалити категорію?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" style="background-color: #3c8dbc;" data-dismiss="modal">Закрити</button>
                <button type="button" class="btn btn-danger delete-confirm">Видалити</button>
            </div>
            <form id="delete-form" action="" method="POST" style="display: none;">
                @csrf
                @method('DELETE')
            </form>
        </div>
    </div>
</div>

<ol class="breadcrumb">
    @foreach($breadcrumbs as $breadcrumb)
        @if($breadcrumb['route'])
            <li><a href="{{ $breadcrumb['route'] }}">{{ $breadcrumb['title'] }}</a></li>
        @else
            <li class="active">{{ $breadcrumb['title'] }}</li>
        @endif
    @endforeach
</ol>
<div class="under-breadcrumbs"></div>

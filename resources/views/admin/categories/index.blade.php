@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">
                    @if(session('success'))
                        <div class="alert alert-success" style="font-size: 1.2em;">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger" style="font-size: 1.2em;">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if(session('warning'))
                        <div class="alert alert-warning" style="font-size: 1.2em;">
                            {{ session('warning') }}
                        </div>
                        @php
                            session()->forget('warning');
                        @endphp
                    @endif

                    @if(isset($warnings) && !empty($warnings))
                        @foreach($warnings as $warning)
                            <div class="alert alert-warning" style="font-size: 1.2em;">
                                {{ $warning }}
                            </div>
                        @endforeach
                    @endif

                    <table class="table table-bordered table-hover" style="font-size: 1.2em;">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Назва категорії</th>
                            <th style="width: 220px;">Що зробити</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td><a href="{{ route('categories.show', $category->slug) }}">{{ $category->name }}</a>
                                </td>
                                <td>
                                    <a style="width: 100px" href="{{ route('categories.edit', $category->slug) }}"
                                       class="btn btn-warning">Редагувати</a>
                                    <form action="{{ route('categories.destroy', $category->slug) }}" method="POST"
                                        style="display: inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <button style="width: 100px" type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop" data-slug="{{ $category->slug }}" data-url="{{ route('categories.destroy', $category->slug) }}">Видалити</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><a style="width: 205px" href="{{ route('categories.create') }}"
                                   class="btn btn-primary pull-right">Додати категорію</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    @include('admin.components.category-delete-modal')

@endsection

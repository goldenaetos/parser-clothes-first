@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" style="font-size: 1.2em;">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form id="categoryForm" action="{{ route('categories.update', $category->slug) }}"
                              method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Назва категорії:</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       value="{{ $category->name }}" placeholder="Enter category name">
                            </div>
                            <button style="width: 100px;" type="submit" class="btn btn-primary">Зберегти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body" style="font-size: 1.2em;">
                    <p><strong>ID:</strong> {{ $category->id }}</p>
                    <p><strong>Назва:</strong> {{ $category->name }}</p>
                    <div>
                        <a href="{{ route('categories.edit', $category->slug) }}" class="btn btn-warning"
                           style="width: 100px; margin-right: 5px;">Редагувати</a>
                        <form action="{{ route('categories.destroy', $category->slug) }}" method="POST"
                              style="display: inline-block;">
                            @csrf
                            @method('DELETE')
                            <button style="width: 100px" type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop" data-slug="{{ $category->slug }}" data-url="{{ route('categories.destroy', $category->slug) }}">Видалити</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.components.category-delete-modal')

@endsection

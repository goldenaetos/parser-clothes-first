@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body" style="font-size: 1.2em;">
                    <p><strong>ID:</strong> {{ $page->id }}</p>
                    <p><strong>URL:</strong> {{ $page->url }}</p>
                    <p><strong>Категорія:</strong> {{ $page->category->name }}</p>
                    <p><strong>Комплект для парсингу:</strong> {{ $page->parsingSet ? $page->parsingSet->set_name : 'N/A' }}</p>
                    <div>
                        <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-warning" style="width: 100px; margin-right: 5px;">Редагувати</a>
                        <form action="{{ route('pages.destroy', $page->id) }}" method="POST" style="display: inline-block;">
                            @csrf
                            @method('DELETE')
                            <button style="width: 100px" type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop" data-id="{{ $page->id }}" data-url="{{ route('pages.destroy', $page->id) }}">Видалити</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.components.page-delete-modal')
@endsection

@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">
                    @if(session('success'))
                        <div class="alert alert-success" style="font-size: 1.2em;">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger" style="font-size: 1.2em;">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if(session('warning'))
                        <div class="alert alert-warning" style="font-size: 1.2em;">
                            {{ session('warning') }}
                        </div>
                        @php
                            session()->forget('warning');
                        @endphp
                    @endif

                    @if(isset($warnings) && !empty($warnings))
                        @foreach($warnings as $warning)
                            <div class="alert alert-warning" style="font-size: 1.2em;">
                                {{ $warning }}
                            </div>
                        @endforeach
                    @endif

                    <table id="pages_table" class="table table-bordered table-hover" style="font-size: 1.2em;">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>URL</th>
                            <th>Назва категорії</th>
                            <th>Назва комплекту</th>
                            <th style="width: 220px;">Що зробити</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{ $page->id }}</td>
                                <td><a href="{{ route('pages.show', $page->id) }}">{{ $page->url }}</a></td>
                                <td>{{ $page->category->name }}</td>
                                <td>{{ $page->parsingSet ? $page->parsingSet->set_name : 'N/A' }}</td>
                                <td>
                                    <a style="width: 100px" href="{{ route('pages.edit', $page->id) }}" class="btn btn-warning">Редагувати</a>
                                    <form action="{{ route('pages.destroy', $page->id) }}" method="POST" style="display: inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <button style="width: 100px" type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop" data-id="{{ $page->id }}" data-url="{{ route('pages.destroy', $page->id) }}">Видалити</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><a style="width: 205px" href="{{ route('pages.create') }}" class="btn btn-primary pull-right">Додати сторінку</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    @include('admin.components.page-delete-modal')
@endsection



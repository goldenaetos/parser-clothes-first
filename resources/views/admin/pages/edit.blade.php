@extends('admin.index')

@section('title', $pageTitle ?? 'AdminLTE')

@section('content')
    <section class="content-header">
        <h1 style="text-align: center; font-size: 2em; font-weight: bold;">
            {{ $pageTitle }}
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" style="font-size: 1.2em;">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('pages.update', $page->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="url">URL</label>
                                <input type="text" class="form-control" id="url" name="url" value="{{ old('url', $page->url) }}" placeholder="Enter page URL" required>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Категорія</label>
                                <select class="form-control" id="category_id" name="category_id" required>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $page->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="parsing_set_id">Комплект для парсингу</label>
                                <select class="form-control" id="parsing_set_id" name="parsing_set_id" required>
                                    <option value="" {{ is_null($page->parsing_set_id) ? 'selected' : '' }}>Не встановлений</option>
                                    @foreach($parsingSets as $parsingSet)
                                        <option value="{{ $parsingSet->id }}" {{ $page->parsing_set_id == $parsingSet->id ? 'selected' : '' }}>{{ $parsingSet->set_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button style="width: 100px;" type="submit" class="btn btn-primary">Зберегти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

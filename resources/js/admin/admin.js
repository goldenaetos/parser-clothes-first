$(document).ready(function() {
    $('#staticBackdrop').on('show.bs.modal', function(event) {
        const button = $(event.relatedTarget);
        const actionUrl = button.data('url');
        const modal = $(this);
        const deleteForm = $('#delete-form');
        deleteForm.attr('action', actionUrl);

        modal.find('.delete-confirm').on('click', function() {
            deleteForm.submit();
        });
    });
});

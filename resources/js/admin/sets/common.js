function addItemToList(inputId, listId, inputName) {
    const input = $('#' + inputId);
    const list = $('#' + listId);
    const value = input.val().trim();

    if (value) {
        const listItem = $('<li>').html(`
            ${value}
            <span class="glyphicon glyphicon-remove-circle red" style="cursor: pointer;" onclick="removeItem(this, '${listId}')"></span>
            <input type="hidden" name="${inputName}[]" value="${value}">
        `);
        list.append(listItem);
        input.val('');
        updateIndexes(listId);
    }
}

function addPageToList(pageUrlInputId, pageCategoryInputId, listId) {
    const pageUrlInput = $('#' + pageUrlInputId);
    const pageCategoryInput = $('#' + pageCategoryInputId);
    const list = $('#' + listId);
    const url = pageUrlInput.val().trim();
    const categoryId = pageCategoryInput.val();
    const categoryName = pageCategoryInput.find('option:selected').text();

    if (url && categoryId) {
        const listItem = $('<li>').html(`
            ${url} - ${categoryName}
            <span class="glyphicon glyphicon-remove-circle red" style="cursor: pointer;" onclick="removeItem(this, '${listId}')"></span>
            <input type="hidden" name="pages[${list.children().length}][url]" value="${url}">
            <input type="hidden" name="pages[${list.children().length}][category_id]" value="${categoryId}">
        `);
        list.append(listItem);
        pageUrlInput.val('');
        pageCategoryInput.prop('selectedIndex', 0);
        updateIndexes(listId);
    }
}

function removeItem(element, listId) {
    $(element).parent().remove();
    updateIndexes(listId);
}

function updateIndexes(listId) {
    const list = $('#' + listId);
    list.children().each((index, element) => {
        $(element).find('input').each(function() {
            const name = $(this).attr('name');
            const newName = name.replace(/\[\d+\]/, `[${index}]`);
            $(this).attr('name', newName);
        });
    });
}

function setupAddItemOnEnter(inputId, buttonId) {
    $('#' + inputId).on('keypress', function (event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            $('#' + buttonId).click();
        }
    });
}

function setupErrorMessageClear() {
    $('input, select').on('input', function() {
        const errorMessage = $('#error-message');
        if (errorMessage.length) {
            errorMessage.hide();
        }
    });
}

function sortCategories(categories) {
    return categories.sort((a, b) => a.name.localeCompare(b.name));
}

$(document).ready(function() {
    const sortedCategories = sortCategories(window.categories);
    const $pageCategoryInput = $('#page_category_input');

    $pageCategoryInput.find('option:not(:first)').remove();

    sortedCategories.forEach(category => {
        $pageCategoryInput.append(new Option(category.name, category.id));
    });

    window.elements.forEach(element => {
        $(`#add_${element.name}`).on('click', function() {
            addItemToList(`${element.name}_input`, `${element.name}_list`, element.name);
        });
        setupAddItemOnEnter(`${element.name}_input`, `add_${element.name}`);
    });

    $('#add_page').on('click', function() {
        addPageToList('page_url_input', 'page_category_input', 'pages_list');
    });

    setupErrorMessageClear();

    $('.category-name').each(function() {
        const categoryId = $(this).data('category-id');
        const category = sortedCategories.find(cat => cat.id === categoryId);
        if (category) {
            $(this).text(category.name);
        }
    });

    const table = $("#adminpanel_table").DataTable({
        "pageLength": 100,
    });

    $('form').on('submit', function() {
        const $form = $(this);
        table.rows({ page: 'all' }).every(function() {
            const $node = $(this.node());
            if (!$node.is(':visible')) {
                $node.find('input, select').each(function() {
                    const $input = $(this);
                    $('<input>').attr({
                        type: 'hidden',
                        name: $input.attr('name'),
                        value: $input.val()
                    }).appendTo($form);
                });
            }
        });
    });

    let selectPlaceholderRemoved = false;
    $pageCategoryInput.on('change', function() {
        if (!selectPlaceholderRemoved) {
            $(this).find('option:first').remove();
            selectPlaceholderRemoved = true;
        }
    });
});

window.addItemToList = addItemToList;
window.addPageToList = addPageToList;
window.setupAddItemOnEnter = setupAddItemOnEnter;
window.setupErrorMessageClear = setupErrorMessageClear;
window.sortCategories = sortCategories;
window.removeItem = removeItem;
window.updateIndexes = updateIndexes;

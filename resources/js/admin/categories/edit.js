$(document).ready(function() {
    const $input = $('#name');
    $input.focus();
    const value = $input.val();
    $input.val('');
    $input.val(value);

    const originalName = value;

    $('#categoryForm').submit(function(event) {
        const currentName = $input.val();

        if (currentName === originalName) {
            event.preventDefault();
            $.ajax({
                url: '/admin/categories/set-warning-message',
                method: 'POST',
                contentType: 'application/json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: JSON.stringify({ warning: 'Category name has not been changed.' }),
                success: function() {
                    window.location.href = '/admin/categories';
                }
            });
        }
    });
});

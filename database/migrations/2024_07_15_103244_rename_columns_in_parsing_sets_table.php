<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsInParsingSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->renameColumn('excluded_full_paragraphs', 'excluded_full_tags');
            $table->renameColumn('excluded_paragraph_starts', 'excluded_tag_starts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->renameColumn('excluded_full_tags', 'excluded_full_paragraphs');
            $table->renameColumn('excluded_tag_starts', 'excluded_paragraph_starts');
        });
    }
}

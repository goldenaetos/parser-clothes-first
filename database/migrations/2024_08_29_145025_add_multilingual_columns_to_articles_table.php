<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->string('title_ru')->nullable()->after('title');
            $table->string('title_uk')->nullable()->after('title_ru');
            $table->json('content_ru')->nullable()->after('content');
            $table->json('content_uk')->nullable()->after('content_ru');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn(['title_ru', 'title_uk', 'content_ru', 'content_uk']);
        });
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->json('keywords_to_skip_article')->after('url_exclusions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->dropColumn('keywords_to_skip_article');
        });
    }
};

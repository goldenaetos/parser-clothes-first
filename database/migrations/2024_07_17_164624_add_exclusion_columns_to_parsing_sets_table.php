<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExclusionColumnsToParsingSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->json('span_classes_to_exclude')->nullable()->after('paragraph_classes_to_exclude');
            $table->json('ul_classes_to_exclude')->nullable()->after('span_classes_to_exclude');
            $table->json('all_elements_classes_to_exclude')->nullable()->after('ul_classes_to_exclude');
            $table->json('all_element_classes_parts_to_exclude')->nullable()->after('all_elements_classes_to_exclude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->dropColumn('span_classes_to_exclude');
            $table->dropColumn('ul_classes_to_exclude');
            $table->dropColumn('all_elements_classes_to_exclude');
            $table->dropColumn('all_element_classes_parts_to_exclude');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->dropColumn(['excluded_paragraph_contents', 'excluded_paragraph_patterns', 'image_tail_to_append']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('parsing_sets', function (Blueprint $table) {
            $table->json('excluded_paragraph_contents')->nullable();
            $table->json('excluded_paragraph_patterns')->nullable();
            $table->string('image_tail_to_append')->nullable();
        });
    }
};

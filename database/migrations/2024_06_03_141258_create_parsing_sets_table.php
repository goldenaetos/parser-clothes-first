<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('parsing_sets', function (Blueprint $table) {
            $table->id();
            $table->string('set_name');
            $table->string('slug');
            $table->string('site_address');
            $table->string('code_start');
            $table->string('code_end');
            $table->string('url_pattern');
            $table->json('url_exclusions')->nullable();
            $table->json('header_classes_to_exclude')->nullable();
            $table->json('paragraph_classes_to_exclude')->nullable();
            $table->json('image_classes_to_exclude')->nullable();
            $table->json('path_to_image_contains_to_exclude')->nullable();
            $table->json('stop_words')->nullable();
            $table->json('excluded_full_tags')->nullable();
            $table->json('excluded_tag_starts')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('parsing_sets');
    }
};

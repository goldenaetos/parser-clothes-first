<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
    )
    ->withMiddleware(function (Middleware $middleware) {
        //
    })
    ->withExceptions(function (Exceptions $exceptions) {
        $exceptions->renderable(function (NotFoundHttpException $e, $request) {
            $previousException = $e->getPrevious();

            if ($previousException instanceof ModelNotFoundException) {
                $model = $previousException->getModel();

                return match ($model) {
                    'App\Models\Admin\Category' => redirect()->route('categories.index')->with('error', 'Категорію не знайдено.'),
                    'App\Models\Admin\Page' => redirect()->route('pages.index')->with('error', 'Сторінку не знайдено.'),
                    'App\Models\Admin\ParsingSet' => redirect()->route('sets.index')->with('error', 'Комплект для парсингу не знайдено.'),
                    default => redirect()->route('admin')->with('error', 'Ресурс не знайдено.'),
                };
            }
        });
    })->create();
